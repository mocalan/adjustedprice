# Documentation on how to execute the application

-   Build image:

```
docker build -t adjustedprice .
```

-   Run image

```
docker run -p 3000:3000 -d adjustedprice
```

-   View on `localhost:3000`

# Design and tradeoffs

The app is based on a Create React App front end and a node/express backend. These choices are mainly for speed and ease of use.

The backend process reads a directory of JSONs in and assigns them to a JS object. Because of the small size this is kept in the memory in a global variable and not written to a file or DB.

The loop that reads the JSONs cannot calculate the adjusted prices since the last CAF is not known. It is thus necessary to run another loop after to fill in the adjusted prices. The process is O(n). There is one approach that can save a bit of computation by reading the files in reverse until the first CAF is found and then loop over the whole dataset just once. This may make it a bit faster in dealing with large data sets but for this application the reduction of code readability and complexity doesn't seem to justify it.

# Testing

Three levels of test:

1. Unit tests with a framework like Jest to ensure each function behaves as desired
2. Integration tests which call API end points with axios calls. Test query parameters and response
3. Dev staging environment with a replica DB and file system where API endpoints are tested on actual data. This should be automated with a tool like Puppeteer to call as many pages and options as possible.

# Monitoring

The levels of exceptions should be monitored at a site level as a baseline. For this system, there are specific failure modes of file not being found, corrupted structure or missing data. Each of these can trigger a different kind of error message. Depending on the error message, the system should trigger an email or SMS message depending on the criticality of the issue. In AWS this can be done with SNS and making sure the process is given the right Task Execution Roles

# Detect missing data points

There should be a DB query that provides trading days. This process can read these and while processing data make sure there is a data point for each day. If not, this can trigger a message to the devs.

# Alert engineers of corrupt input data

The line where the data is read:

```
const fileObj = require(filePath)[0];
```

can be placed in a try-catch block and issue and alert to the devs.

# Alert engineers of infrastructure outage

For the infrastructure this app is running cloud monitoring features can be used. In AWS this can be done my CloudWatch. The application should include (and in some deployments like ECS/EKS or ASG has to include) a health check end point. This can trigger and SNS message that can be routed to email and SMS.

For infrastructure outage of the data vendor can be found while accessing the FTP site or other data source. A message can be sent to the devs for this exception.

const fs = require('fs');
const path = require('path');
const express = require('express');
const app = express();

const PORT = 3000;
const API_MAX_LIMIT = 5000;

inputRelPath = '../inputs/json/';
inputFolder = path.join(__dirname, inputRelPath);

// Global variable in place of a database
let processedData;

try {
    fs.readdir(inputFolder, async (err, files) => {
        if (err) throw err;

        // There is an inherent assumption here that the files are accessed
        // in a date ordered fashion and all files are under a single assetId
        // For the real case the assetId would be a DB lookup on either the whole
        // directory or per file level. If it is per file lookup, some kind of
        // cashing is probably necessary. I will assume one can get the following
        // from the cashed DB lookup:
        cachedAssetDetail = {
            4000123: {
                KID: 'mf-5s7ebl',
                ticker: 'LALDX',
                qualifiedTicker: 'LALDX:US',
                ISIN: 'UA123456789',
            },
        };

        // Initialize adjutment factor. This should be the last value available
        // on our DB or 1 if not available
        let adjFactor = 1;

        processedData = new Array(files.length);
        for (let i = 0; i < files.length; i++) {
            // Read the file into a JS object
            const file = files[i];
            const filePath = path.join(__dirname, inputRelPath, file);
            const fileObj = require(filePath)[0];
            // Note: some grannular error handling here may help with monitoring
            // key failure modes and reducing unnecessary dev resources spent on
            // checks and updates. For the purposes here, I am letting the main
            // try-catch loop handle these

            // Update adjustment factor if available
            adjFactor = fileObj.adjustmentFactor || adjFactor;

            processedData[i] = {
                ...fileObj,
                ...cachedAssetDetail[fileObj.assetId],
                caf: adjFactor,
            };
        }

        // Run loop one more time to calculate adjusted prices
        for (let i = 0; i < files.length; i++) {
            processedData[i].adjustedNav = (processedData[i].nav * processedData[i].caf) / adjFactor;
        }

        // Here is where we need to update the historical database values
        // with the new adjustment factors. A check if there was any updates on the new
        // data will save unnecessary updates.
    });
} catch (error) {
    // Implement monitoring features to alert developers if
    // any file read operation is not going as planned
    console.error(error);
}

app.get('/data', (req, res) => {
    const { startDate, endDate, kid, limit, start } = req.query;
    // Here is the place to validate the inputs which is outside the scope of this demo
    // Since we have only one KID, we will assume that a db call is made with time range and KID

    // Pagination is handled by the limit and start query parameters. Set limit to max if n
    // Pagination will not be implemented per the question specs
    const resLimit = !limit || limit > API_MAX_LIMIT ? API_MAX_LIMIT : limit;

    // For start and end dates I will use a simple string comparison. The actual program should
    // convert these to date format used in the database and query accordingly
    const resStartDate = startDate || 0;
    const resEndDate = endDate || 9;

    const resData = processedData.filter((data) => resStartDate <= data.date && data.date < resEndDate);
    res.send(resData.map((data) => ({ date: data.date, nav: data.nav, adjustedNav: data.adjustedNav })));
});

app.use(express.static('../frontend/build/'));

app.listen(PORT, () => {
    console.log(`Koyfin app listening at http://localhost:${PORT}`);
});

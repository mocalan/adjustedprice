FROM node:14.16.0

# Create a user to run the server
ENV USER="koyfin"
ENV USER_DIR="/home/${USER}"
ENV BACKEND_DIR="${USER_DIR}/backend"
ENV FRONTEND_DIR="${USER_DIR}/frontend"
ENV INPUTS_DIR="${USER_DIR}/inputs"
RUN adduser --home ${USER_DIR} ${USER}

USER ${USER}
RUN mkdir -p ${BACKEND_DIR} \
    && echo ${BACKEND_DIR} ${USER}
RUN mkdir -p ${INPUTS_DIR} \
    && echo ${INPUTS_DIR} ${USER}

# copy frontend and build 
RUN mkdir ${FRONTEND_DIR}
WORKDIR ${FRONTEND_DIR}
COPY --chown=${USER} ./frontend/package*.json ${FRONTEND_DIR}/
RUN npm install

COPY --chown=${USER} ./frontend/. ${FRONTEND_DIR}/.
RUN npm run build

# re-install node dependencies only if they changed
WORKDIR ${BACKEND_DIR}
COPY --chown=${USER} ./backend/package*.json ${BACKEND_DIR}/
RUN npm install 

# copy rest of files needed for build
COPY --chown=${USER} ./backend/. ${BACKEND_DIR}/.

# Copy any other files needed by the backend
COPY --chown=${USER} ./inputs/. ${INPUTS_DIR}/

# Execution configuration
WORKDIR ${BACKEND_DIR}
EXPOSE 3000

ENTRYPOINT [ "node", "index.js" ]

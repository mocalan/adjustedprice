import './App.css';
import Plot from 'react-plotly.js';
import axios from 'axios';
import { useEffect, useState } from 'react';

function App() {
    const [data, setData] = useState({ date: [], nav: [], adjustedNav: [] });

    useEffect(() => {
        async function fetchAPI() {
            try {
                const rawData = await axios
                    .get('/data', {
                        params: {
                            startDate: '2019-01-01',
                            endDate: '2021-02-28',
                        },
                    })
                    .then((res) => res.data);
                const plotData = rawData.reduce(
                    (acc, data) => ({
                        date: acc.date.concat(data.date),
                        nav: acc.nav.concat(data.nav),
                        adjustedNav: acc.adjustedNav.concat(data.adjustedNav),
                    }),
                    { date: [], nav: [], adjustedNav: [] }
                );
                setData(plotData);
            } catch (error) {
                console.error(error);
            }
        }
        fetchAPI();
    }, []);

    return (
        <div className="App">
            <header className="App-header">
                <Plot
                    data={[
                        {
                            x: data.date,
                            y: data.adjustedNav,
                            type: 'scatter',
                            mode: 'lines',
                            marker: { color: 'blue' },
                            name: 'Adjusted NAV',
                        },
                        {
                            x: data.date,
                            y: data.nav,
                            type: 'scatter',
                            mode: 'lines',
                            marker: { color: 'red' },
                            name: 'NAV',
                        },
                    ]}
                    layout={{ width: 640, height: 480, title: 'LALDX:US' }}
                />
                <p>Koyfin demo</p>
            </header>
        </div>
    );
}

export default App;
